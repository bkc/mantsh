FROM nginx:1.22.0-alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY movealong.jpg /www/
COPY index.html /www/
COPY upload/ /www/upload/
COPY fiesta/ /www/fiesta/


